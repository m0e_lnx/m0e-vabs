#!/usr/bin/bash
# This script assumes it will be launched within "/NAME/VERSION/src" dir.
# With all sources in "src" Your Vector Linux .txz package, slack-desc,
# and slack-required will be found in "VERSION" dir. The extraction and
# build will be in a temp dir created in "NAME" dir, and then removed on exit.
# Comment out second to last line to keep this dir intact.
#
# This Template was compiled from the contributions of many users of the Vector
# Linux forum at http://forum.vectorlinux.com and from tidbits collected 
# from all over the internet. 
#
# Generated by sbbuilder-0.4.15, written by Rodrigo Bistolfi 
# (rbistolfi) and Raimon Grau Cuscó (Kidd) for VectorLinux.

# Copyright 2006, 2007, 2008, 2009, 2010, 2011, 2012  Patrick J. Volkerding, Sebeka, MN, USA
# All rights reserved.
#
# Redistribution and use of this script, with or without modification, is
# permitted provided that the following conditions are met:
#
# 1. Redistributions of this script must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
#
#  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED
#  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
#  MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO
#  EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
#  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
#  PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
#  OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
#  WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
#  OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
#  ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

# This script was written using the one from slackbuilds.org as a reference,
# so thanks to Adis Nezirovic ( adis _at_ linux.org.ba ) for the original work.

# Modified for VL by rbistolfi

NAME="httpd"            #Enter package Name!
VERSION=${VERSION:-"2.4.7"}      #Enter package Version!
VER=$(echo $VERSION|sed 's/-/_/') #this fixes - in version
VL_PACKAGER=${VL_PACKAGER:-"rbistolfi"}   #Enter your Name!
LINK=${LINK:-"http://apache.org/dist/${NAME}/${NAME}-${VERSION}.tar.gz"}  #Enter URL for package here!


#SYSTEM VARIABLES
#----------------------------------------------------------------------------
BUILDNUM=${BUILDNUM:-"1"}
VL_VERSION=${VL_VERSION:-"$(ls /var/log/packages/|grep vlconfig2|cut -d "-" -f4|cut -c 2-5)"}
BUILD=${BUILD:-"$BUILDNUM""$VL_VERSION"}
ARCH=${ARCH:-"$(uname -m)"}
CONFIG_OPTIONS=${CONFIG_OPTIONS:-"--enable-layout=Slackware-FHS \
	--with-apr=/usr \
	--with-apr-util=/usr \
	--enable-mods-shared=all \
	--enable-so \
	--enable-mpms-shared=all \
	--enable-pie \
	--enable-cgi \
	--with-pcre \
	--enable-ssl \
	--enable-rewrite \
	--enable-vhost-alias \
	--enable-proxy \
	--enable-proxy-http \
	--enable-proxy-ftp \
	--enable-cache \
	--enable-mem-cache \
	--enable-file-cache \
	--enable-disk-cache \
	--enable-dav \
	--enable-ldap \
	--enable-authnz-ldap \
	--enable-authn-anon \
	--enable-authn-alias "}
LDFLAG_OPTIONS=${LDFLAG_OPTIONS:-""}
ADDRB=${ADDRB:-""} #Add deps that need to be added to the slack-required file here
EXRB=${EXRB:-""} #Add deps that need to be excluded from the slack-required file here
MAKEDEPENDS=${MAKEDEPENDS:-"pcre openldap apr apr-util cyrus-sasl db libxml2 lua"} #Add deps needed TO BUILD this package here.
#----------------------------------------------------------------------------

# DO NOT EXECUTE if NORUN is set to 1
if [ "$NORUN" != "1" ]; then

#SETUP PACKAGING ENVIRONMENT
#--------------------------------------------
CWD=$(pwd)
cd ../
RELEASEDIR=$(pwd)
cd $CWD
mkdir -p $RELEASEDIR/tmp
TMP=$RELEASEDIR/tmp
PKG=$TMP/package-$NAME
#--------------------------------------------



if [ $UID != 0 ]; then
echo "You are not authorized to run this script. Please login as root"
exit 1
fi

if [ ! -x /usr/bin/requiredbuilder ]; then
echo "Requiredbuilder not installed, or not executable."
exit 1
fi

if [ $VL_PACKAGER = "YOURNAME" ]; then
echo 'Who are you?
Please edit VL_PACKAGER=${VL_PACKAGER:-YOURNAME} in this script.
Change the word "YOURNAME" to your VectorLinux packager name.
You may also export VL_PACKAGER, or call this script with
VL_PACKAGER="YOUR NAME HERE"'
exit 1
fi


#CFLAGS SETUP
#--------------------------------------------
if [[ "$ARCH" = i?86 ]]; then
ARCH=i586
SLKCFLAGS="-O2 -march=i586 -mtune=i686"
CONFIGURE_TRIPLET="i486-vector-linux"
LIBDIRSUFFIX=""
elif [ "$ARCH" = "x86_64" ]; then
SLKCFLAGS="-O2 -fpic"
CONFIGURE_TRIPLET="x86_64-vlocity-linux"
LIBDIRSUFFIX="64"
elif [ "$ARCH" = "powerpc" ]; then
  SLKCFLAGS="-O2"
  CONFIGURE_TRIPLET="powerpc-vlocity-linux"
  LIBDIRSUFFIX=""
fi

export CFLAGS="$SLKCFLAGS $CFLAG_OPTIONS"
export CXXFLAGS=$CFLAGS
export LDFLAGS="$LDFLAGS $LDFLAG_OPTIONS"
#--------------------------------------------


#GET THE SOURCE
#--------------------------------------------
for SRC in $(echo $LINK);do
if [ ! -f $CWD/$(basename $SRC) ]
then
        wget --no-check-certificate -c $SRC
fi
done
#--------------------------------------------


rm -rf $PKG
cd $TMP
rm -rf $NAME-$VERSION


#EXTRACT SOURCES
#-----------------------------------------------------
echo "Extracting source..."
tar xvf $CWD/$NAME-$VERSION.tar.* || exit 1
#this moves whatever was extracted to the std dirname we are expecting
mv * $NAME-$VERSION &> /dev/null 2>&1
mkdir -p $PKG
#-----------------------------------------------------


cd $TMP/$NAME-$VERSION


#PATCHES
#-----------------------------------------------------
# Put any Patches here *NOTE this only works if all 
# your patches use the -p1 strip option!
#-----------------------------------------------------
for i in $CWD/patches/*;do
  patch -p1 <$i
  mkdir -p $PKG/usr/doc/$NAME-$VERSION/patches/
  cp $i $PKG/usr/doc/$NAME-$VERSION/patches/
done
#-----------------------------------------------------
 
 

#SET PERMISSIONS
#-----------------------------------------
echo "Setting permissions..."
chown -R root:root .
find . -perm 664 -exec chmod 644 {} \;
find . -perm 777 -exec chmod 755 {} \;
find . -perm 2777 -exec chmod 755 {} \;
find . -perm 775 -exec chmod 755 {} \;
find . -perm 2755 -exec chmod 755 {} \;
find . -perm 774 -exec chmod 644 {} \;
find . -perm 666 -exec chmod 644 {} \;
find . -perm 600 -exec chmod 644 {} \;
find . -perm 444 -exec chmod 644 {} \;
find . -perm 400 -exec chmod 644 {} \;
find . -perm 440 -exec chmod 644 {} \;
find . -perm 511 -exec chmod 755 {} \;
find . -perm 711 -exec chmod 755 {} \;
find . -perm 555 -exec chmod 755 {} \;
#-----------------------------------------



#CONFIGURE & MAKE
#----------------------------------------------------------------------
# If you are building a KDE-related app, then change the following
# arguments in the script below:
# --prefix=$(kde-config -prefix) \
# --sysconfdir=/etc/kde \
#
# Making these changes will ensure that your package will build in the
# correct path and that it will work seamlessly within the KDE environment.
#
#-----------------------------------------------------------------------

# Fix config.layout to use lib${LIBDIRSUFFIX}:
sed -i -e "s#lib/httpd#lib${LIBDIRSUFFIX}/httpd#" config.layout

# If /var/run becomes a tmpfs or a link to /run, subdirectories could be a problem.
# Just use /var/run rather than /var/run/httpd.
sed -i -e "s#/run/httpd#/run#" config.layout

echo "Configuring source..."
./configure --prefix=/usr \
  --libdir=/usr/lib${LIBDIRSUFFIX} \
  --infodir=/usr/info \
  --sysconfdir=/etc/httpd \
  --localstatedir=/var \
  --mandir=/usr/man \
  --with-included-gettext \
  --disable-debug \
  --program-prefix="" \
  --program-suffix="" \
  --build=$CONFIGURE_TRIPLET \
  $CONFIG_OPTIONS || exit 1

make || exit 1

make install DESTDIR=$PKG || exit 1


#######################################################################
#Miscellenious tweaks and things outside a normal ./configure go here #
#######################################################################

# Tweak default apache configuration
( cd $PKG
  zcat $CWD/httpd.runasapache.diff.gz | patch -p1 --verbose || exit 1
  # mod_proxy_balancer should be commented out, as otherwise httpd
  # will not start without additional configuration:
  sed -i "s/^LoadModule proxy_balancer_module/#LoadModule proxy_balancer_module/g" $PKG/etc/httpd/httpd.conf
  # This module issues a warning unless some non-default modules are loaded:
  sed -i "s/^LoadModule lbmethod_heartbeat_module/#LoadModule lbmethod_heartbeat_module/g" $PKG/etc/httpd/httpd.conf
  rm -f $PKG/etc/httpd/httpd.conf~ $PKG/etc/httpd/httpd.conf.orig
) || exit 1
# Change config files to .new:
( cd $PKG/etc/httpd
  mv httpd.conf httpd.conf.new
  for file in extra/*; do
  mv $file "${file}.new"
done
)

cat << EOF >> $PKG/etc/httpd/httpd.conf.new

# Uncomment the following line to enable PHP:
#
#Include /etc/httpd/mod_php.conf

# Uncomment the following lines (and mod_dav above) to enable svn support:
#
#LoadModule dav_svn_module lib${LIBDIRSUFFIX}/httpd/modules/mod_dav_svn.so
#LoadModule authz_svn_module lib${LIBDIRSUFFIX}/httpd/modules/mod_authz_svn.so

EOF

rmdir $PKG/var/log/httpd

mkdir -p $PKG/etc/rc.d
cat $CWD/rc.httpd > $PKG/etc/rc.d/rc.httpd.new

mkdir -p $PKG/etc/logrotate.d
cat $CWD/logrotate.httpd > $PKG/etc/logrotate.d/httpd.new

mkdir -p $PKG/install
zcat $CWD/doinst.sh.gz > $PKG/install/doinst.sh

mv $PKG/srv/httpd $PKG/var/www
( cd $PKG/srv
  ln -sf /var/www .
  ln -sf /var/www httpd
)

mv $PKG/var/www/htdocs/index.html $PKG/var/www/htdocs/index.html.new

mkdir -p $PKG/usr/doc/$PKGNAM-$VERSION/
cp -a \
ABOUT_APACHE Apache.dsw BuildBin.dsp CHANGES INSTALL InstallBin.dsp LAYOUT LICENSE NOTICE NWGNUmakefile README* ROADMAP VERSIONING \
$PKG/usr/doc/$NAME-$VERSION
cat $CWD/$NAME.SlackBuild > $PKG/usr/doc/$NAME-$VERSION/$NAME.SlackBuild

#----------------------------------------------------------------------

if [ -d $PKG/usr/share/man ];then
  mkdir -p $PKG/usr/man
  mv $PKG/usr/share/man/* $PKG/usr/man
  rm -rf $PKG/usr/share/man
fi
find $PKG/usr/man -type f -exec gzip -9 {} \;


mkdir -p $PKG/install


#if there is a slack-desc in src dir use it
if test -f $CWD/slack-desc; then
cp $CWD/slack-desc $RELEASEDIR/slack-desc
else
# This creates the white space in front of "handy-ruler" in slack-desc below.

LENGTH=$(expr length "$NAME")
SPACES=0
SHIM=""
until [ "$SPACES" = "$LENGTH" ]; do
SHIM="$SHIM "
let SPACES=$SPACES+1
done

# Fill in the package summary between the () below.
# Then package the description, License, Author and Website.
# There may be no more then 11 $NAME: lines in a valid slack-desc.

cat > $RELEASEDIR/slack-desc << EOF
# HOW TO EDIT THIS FILE:
# The "handy ruler" below makes it easier to edit a package description.  Line
# up the first '|' above the ':' following the base package name, and the '|'
# on the right side marks the last column you can put a character in.  You must
# make exactly 11 lines for the formatting to be correct.  It's also
# customary to leave one space after the ':'.

$SHIM|-----handy-ruler------------------------------------------------------|
$NAME: The Apache HTTP Server
$NAME:
$NAME: Apache is an HTTP server designed as a plug-in replacement for the
$NAME: NCSA HTTP server. 
$NAME: Apache is the most popular web server in the known universe; over
$NAME: half of the servers on the Internet are running Apache or one of
$NAME: its variants.
$NAME:
$NAME: License: GPL
$NAME: Authors: http://www.apache.org/
$NAME: Website: http://httpd.apache.org/

EOF
fi
cat >> $RELEASEDIR/slack-desc << EOF



#----------------------------------------
BUILDDATE: $(date)
PACKAGER:  $VL_PACKAGER
HOST:      $(uname -srm)
DISTRO:    $(cat /etc/vector-version)
CFLAGS:    $CFLAGS
LDFLAGS:   $LDFLAGS
CONFIGURE: $(awk "/\\$\ \.\/configure\ /" $TMP/$DIRNAME/config.log)

EOF

cat $RELEASEDIR/slack-desc > $PKG/install/slack-desc

#STRIPPING
#------------------------------------------------------------------------------------------------------------------
cd $PKG
echo " "
echo "Stripping...."
echo " "
find . | xargs file | grep "executable" | grep ELF | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null
find . | xargs file | grep "shared object" | grep ELF | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null
#------------------------------------------------------------------------------------------------------------------


#FINISH PACKAGE
#--------------------------------------------------------------
echo "Finding dependencies..."
ADD="$ADDRB" EXCLUDE="$EXRB" requiredbuilder -v -y -s $RELEASEDIR $PKG
echo "Creating package $NAME-$VERSION-$ARCH-$BUILD.txz"
makepkg -l y -c n $RELEASEDIR/$NAME-$VERSION-$ARCH-$BUILD.txz

cd $CWD
echo "Cleaning up temp files..." && rm -rf $TMP
echo "Package Complete"
fi
#--------------------------------------------------------------

# vim: set tabstop=4 shiftwidth=4 foldmethod=marker : ##
