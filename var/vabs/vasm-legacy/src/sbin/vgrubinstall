#!/bin/sh
# vgrubinstall
# This is an attempt to make a grub installer for VL
# Most of the code below was taken from vgrubconf
# 
# Uel Archuletta , 2011
# License: GNU GPL
#


vdir=$(dirname $0)
. $vdir/vasm-functions

check_root

TMP=/var/log/setup/tmp
mkdir -p $TMP

# VARIABLES
TP_X=${1:-/}
if [ "$TP_X" = "/" ]; then
    ROOT_DIR=""
else
    ROOT_DIR=$TP_X
fi

BOOT_DIR=$ROOT_DIR/boot
SPLASH=""
CONSOLETYPE="standard"

ROOT_DEVICE=$2
if [ -z "$ROOT_DEVICE" ]; then
  ROOT_DEVICE=`cat /etc/fstab | awk '{if ($2 == "/") {print $1}}' `
  # This must be successful
  if [ -z "$ROOT_DEVICE" ]; then
    ROOT_DEVICE=`mount | awk '{if ($3 == "/") {print $1}}'`
  fi
fi

menu_boot()
{
$DCMD --backtitle "$BACKTITLE" --title "GRUB INSTALL" --menu "\n
GRUB (Linux Loader) allows you to boot various operating systems on
multiple partitions. To use it, you must install GRUB on either:\n
 1. The Master Boot Record of your first hard drive. Use this unless\n
    you are using another boot manager (e.g GRUB, PQMagic, NT Loader)\n
 2. The boot sector of your Linux partition. Then you need to:\n
      a) make the partition bootable (using fdisk for example).\n
      b) use another boot manager to boot this partition.\n
 3. A formatted floppy disk. The safest and the slowest.\n
Which option would you like?" \
0 0 4 \
"MBR" "install to Master Boot Record (simplest)" \
"SECTOR" "install to boot sector (needs extra manual steps)" \
"FLOPPY" "install to a formatted floppy in /dev/fd0 (A:)" \
"SKIP" "do not install GRUB2" \
2> $freply

 result=$?
 [ $result != 0 ] && return $result

 reply="$(cat $freply)"
case "$reply" in
  ##if the user chose MBR check to see if more than 1 mbr is available if only 1 use it 
  MBR)
   HardDisk=$(fdisk -l | grep Disk | cut -d " " -f 2 | cut -c 1-8|grep ^/dev)
   echo $HardDisk
   x=$(echo $HardDisk | wc -w)
   echo $x
   if [ $x != 1 ]; then  
     j="0"
     for i in $HardDisk; do
     hd[$j]="$i"
     ((j++))
     done
     echo $j
     #exit
     $DCMD --backtitle "$BACKTITLE" --title "MBR INSTALL" --menu "\n
     GRUB (Linux Loader) install grub to mbr \n
     you chose to install grub to the MBR, and \n
     you have multiple hard disks\n
     we need to know where you would like the mbr to be written \n
     Choose which hard disk you would like to use" \
     0 0 4 \
     ${hd[0]} ${hd[0]} \
     ${hd[1]} ${hd[1]} \
     ${hd[2]} ${hd[2]} \
     ${hd[3]} ${hd[3]} \
     ${hd[4]} ${hd[4]} \
     ${hd[5]} ${hd[5]} \
     2> $freply

     result=$?
     [ $result != 0 ] && return $result

     GRUB_TARGET="`cat $freply`"
   else
    GRUB_TARGET="$HardDisk"
   fi
  ;;
 SECTOR)
  GRUB_TARGET="$ROOT_DEVICE"
  ;;
 FLOPPY)
  GRUB_TARGET="/dev/fd0"
  ;;
 *)
  clean_exit
  ;;
esac 
return 0
}

# Menu to check if we want to use VESA framebuffer support:
menu_fb() {

  # if FB is not supported, skip
  if ! cat /proc/devices | grep "29 fb" 1> /dev/null ; then
    return $1
  fi
  $DCMD --backtitle "$BACKTITLE" --title "GRUB FRAME BUFFER CONSOLE" \
  --default-item $CONSOLETYPE --menu "\n
Choose a resolution for grub, and bootsplash. The most common is the first option. \n 
If you are not sure which option to choose then just click enter here. \n
bootsplash med and low are for older, or lower resolution systems. \n
bootsplash-Xhigh will not work on all systems" 0 0 3  \
  "bootsplash-high" "Show graphical image during boot, 1024x768" \
  "bootsplash-med" "Show graphical image during boot, 800x600" \
  "bootsplash-low" "Show graphical image during boot, 640x480" \
  "bootsplash-Xhigh" "Show graphical image during boot, 1280x1024" \
  2> $freply
  
  result=$?
  [ $result != 0 ] && return $result
  
  CONSOLETYPE="`cat $freply`"
  if [ "$CONSOLETYPE" = "bootsplash-high" ]; then
    sed -i s"/GRUB_GFXMODE=.*/GRUB_GFXMODE=1024x768x16/" /etc/default/grub
  elif [ "$CONSOLETYPE" = "bootsplash-med" ]; then
    sed -i s"/GRUB_GFXMODE=.*/GRUB_GFXMODE=800x600x16/" /etc/default/grub
  elif [ "$CONSOLETYPE" = "bootsplash-med" ]; then
    sed -i s"/GRUB_GFXMODE=.*/GRUB_GFXMODE=640x480x16/" /etc/default/grub
  elif [ "$CONSOLETYPE" = "bootsplash-Xhigh" ]; then
    sed -i s"/GRUB_GFXMODE=.*/GRUB_GFXMODE=1280x1024x16/" /etc/default/grub
  fi
  return 0
}

menu_param()
{

$WCMD --backtitle "$BACKTITLE" --title "GRUB OPTIONAL PARAMETERS" \
--inputbox "\n
This is a bit technical but optional. So you may skip it without
reading on. Hit <OK> now.\n\n
Still with me? Alright, this dialog allows you to set boot parameters.
For example, if you want to try hibernation, type:\n
  resume=/dev/hdax : where /dev/hdax is the swap partition\n
If your system hung during initialization, bypass it by using:\n
  nohotplug    : do not set up hotplug devices (USB/PCI)\n
  nohwclock    : do not use realtime clock\n
  noapic       : turn off Advanced Programmable Interrupt Controller\n
You may enter many parameters separated by space. Empty is fine:" \
0 0 2> $freply

result=$?
[ $result != 0 ] && return $result
 
     APPEND="`cat $freply`"
    return 0
  
}

############################################################
# Here we go 
menu_finish()
{

#infobox "Installing GRUB into $GRUB_TARGET ..." "GRUB"
# Done, now we must install grub:
#infobox "Making grub.cfg ..." "GRUB"
COUNT=1
rm /boot/grub/grub.cfg
(
while [ ! -f /boot/grub/grub.cfg ]
#while [ $COUNT -lt 101 ]
do
	if [ $COUNT = 1 ];then
		grub-install $GRUB_TARGET &
	fi
	if [ $COUNT = 10 ];then
		grub-mkconfig -o /boot/grub/grub.cfg &
	fi
	if [ $COUNT -lt 15 ];then
	 echo $COUNT
	 echo "XXX"
	 echo "Installing GRUB into $GRUB_TARGET"
	 echo "XXX"
	else
	 echo $COUNT
	 echo "XXX"
	 echo "Generating Grub.cfg"
	 echo "XXX"
 	fi 
	COUNT=`expr $COUNT + 1`
	sleep 1
done
) | $DCMD --title "Installing GRUB2" --gauge "Hi, this is a gauge widget" 10 70 0
# cat /etc/grub.conf
infobox "SUCCESS GRUB has been installed /
DONE"
 
clean_exit 0
}


############################
# MAIN MENU
wizard menu_boot menu_fb menu_finish

