#!/bin/sh
# Copyright 2006, 2007, 2008, 2009, 2010  Patrick J. Volkerding, Sebeka, MN, USA
# All rights reserved.
#
# Redistribution and use of this script, with or without modification, is
# permitted provided that the following conditions are met:
#
# 1. Redistributions of this script must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
#
#  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED
#  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
#  MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO
#  EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
#  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
#  PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
#  OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
#  WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
#  OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
#  ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


NAME="cxxlibs"
VERSION=${VERSION:-6.0.14}
VL_PACKAGER=${VL_PACKAGER:-"stretchedthin"}   #Enter your Name!
LINK=${LINK:-""}  #Enter URL for package here!

#SYSTEM VARIABLES
#----------------------------------------------------------------------------
BUILDNUM=${BUILDNUM:-"2"}
VL_VERSION=${VL_VERSION:-"$(ls /var/log/packages/|grep vlconfig2|cut -d "-" -f4|cut -c 2-5)"}
BUILD=${BUILD:-"$BUILDNUM""$VL_VERSION"}
ARCH=${ARCH:-"$(uname -m)"}
CONFIG_OPTIONS=${CONFIG_OPTIONS:-""}
LDFLAG_OPTIONS=${LDFLAG_OPTIONS:-""}
ADDRB=${ADDRB:-""} #Add deps that need to be added to the slack-required file here
EXRB=${EXRB:-""} #Add deps that need to be excluded from the slack-required file here
MAKEDEPENDS=${MAKEDEPENDS:-""} #Add deps needed TO BUILD this package here.
#----------------------------------------------------------------------------

# Automatically determine the architecture we're building on:
if [ -z "$ARCH" ]; then
  case "$( uname -m )" in
    i?86) export ARCH=i586 ;;
    arm*) export ARCH=arm ;;
    # Unless $ARCH is already set, use uname -m for all other archs:
       *) export ARCH=$( uname -m ) ;;
  esac
fi

if [ "$ARCH" = "x86_64" ]; then
  LIBDIRSUFFIX="64"
else
  LIBDIRSUFFIX=""
fi

CWD=$(pwd)
cd ../
RELEASEDIR=$(pwd)
cd $CWD
mkdir -p $RELEASEDIR/tmp
TMP=$RELEASEDIR/tmp
PKG=$TMP/package-${NAME}
rm -rf $PKG
mkdir -p $TMP $PKG

cd $PKG
cat $CWD/symlinks-to-tracked-libs${LIBDIRSUFFIX} | while read library ; do
  ( mkdir -p $(dirname $library | cut -b2- )
    cd $(dirname $library | cut -b2- )
    rm -f $(basename $library)
    cp -a $library .
    rm -f $(readlink $library)
    cp -a $(dirname $library)/$(readlink $library) .
  )
done

mkdir -p $PKG/install
cat $CWD/slack-desc > $PKG/install/slack-desc

cd $PKG

/sbin/makepkg -l y -c n $n $RELEASEDIR/${NAME}-$VERSION-$ARCH-$BUILD.txz

rm -rf $TMP
