#!/usr/bin/bash
# This script assumes it will be launched within "/NAME/VERSION/src" dir.
# With all sources in "src" Your Vector Linux .txz package, slack-desc,
# and slack-required will be found in "VERSION" dir. The extraction and
# build will be in a temp dir created in "NAME" dir, and then removed on exit.
# Comment out second to last line to keep this dir intact.
#
# This Template was compiled from the contributions of many users of the Vector
# Linux forum at http://forum.vectorlinux.com and from tidbits collected 
# from all over the internet. 
#
# Generated by sbbuilder-0.4.14.1, written by Rodrigo Bistolfi 
# (rbistolfi) and Raimon Grau Cuscó (Kidd) for VectorLinux.
#
# Please put your name below if you add some original scripting lines.
# AUTHORS = 

NAME="llvm"            #Enter package Name!
VERSION=${VERSION:-"3.2"}      #Enter package Version!
VER=$(echo $VERSION|sed 's/-/_/') #this fixes - in version
VL_PACKAGER=${VL_PACKAGER:-"hata_ph"}   #Enter your Name!
LINK=${LINK:-"http://llvm.org/releases/$VERSION/$NAME-$VERSION.src.tar.gz"}  #Enter URL for package here!


#SYSTEM VARIABLES
#----------------------------------------------------------------------------
BUILDNUM=${BUILDNUM:-"2"}
VL_VERSION=${VL_VERSION:-"$(ls /var/log/packages/|grep vlconfig2|cut -d "-" -f4|cut -c 2-5)"}
BUILD=${BUILD:-"$BUILDNUM""$VL_VERSION"}
ARCH=${ARCH:-"$(uname -m)"}
CONFIG_OPTIONS=${CONFIG_OPTIONS:-""}
LDFLAG_OPTIONS=${LDFLAG_OPTIONS:-""}
ADDRB=${ADDRB:-""} #Add deps that need to be added to the slack-required file here
EXRB=${EXRB:-""} #Add deps that need to be excluded from the slack-required file here
MAKEDEPENDS=${MAKEDEPENDS:-""} #Add deps needed TO BUILD this package here.
#----------------------------------------------------------------------------

# DO NOT EXECUTE if NORUN is set to 1
if [ "$NORUN" != "1" ]; then

#SETUP PACKAGING ENVIRONMENT
#--------------------------------------------
CWD=$(pwd)
cd ../
RELEASEDIR=$(pwd)
cd $CWD
mkdir -p $RELEASEDIR/tmp
TMP=$RELEASEDIR/tmp
PKG=$TMP/package-$NAME
#--------------------------------------------



if [ $UID != 0 ]; then
   echo "You are not authorized to run this script. Please login as root"
   exit 1
fi

if [ ! -x /usr/bin/requiredbuilder ]; then
   echo "Requiredbuilder not installed, or not executable."
   exit 1
fi

if [ $VL_PACKAGER = "YOURNAME" ]; then
   echo 'Who are you?
   Please edit VL_PACKAGER=${VL_PACKAGER:-YOURNAME} in this script.
   Change the word "YOURNAME" to your VectorLinux packager name.
   You may also export VL_PACKAGER, or call this script with
   VL_PACKAGER="YOUR NAME HERE"'
   exit 1
fi


#CFLAGS SETUP
#--------------------------------------------
if [[ "$ARCH" = i?86 ]]; then
  ARCH=i586
  SLKCFLAGS="-O2 -march=i586 -mtune=i686"
  CONFIGURE_TRIPLET="i586-vector-linux"
  LIBDIRSUFFIX=""
elif [ "$ARCH" = "x86_64" ]; then
  SLKCFLAGS="-O2 -fpic"
  CONFIGURE_TRIPLET="x86_64-vlocity-linux"
  LIBDIRSUFFIX="64"
elif [ "$ARCH" = "powerpc" ]; then
  SLKCFLAGS="-O2"
  CONFIGURE_TRIPLET="powerpc-vlocity-linux"
  LIBDIRSUFFIX=""
fi

export CFLAGS="$SLKCFLAGS $CFLAG_OPTIONS"
export CXXFLAGS=$CFLAGS
export LDFLAGS="$LDFLAGS $LDFLAG_OPTIONS"
#--------------------------------------------


#GET THE SOURCE
#--------------------------------------------
for SRC in $(echo $LINK);do
if [ ! -f $CWD/$(basename $SRC) ]
then
        wget --no-check-certificate -c $SRC
fi
done
#--------------------------------------------

CLANG=${CLANG:-yes}
CLANGLINK=${CLANGLINK:-"http://llvm.org/releases/$VERSION/clang-$VERSION.src.tar.gz"}  #Enter URL for package here!

rm -rf $PKG
cd $TMP
rm -rf $NAME-$VERSION


#EXTRACT SOURCES
#-----------------------------------------------------
echo "Extracting source..."
tar xvf $CWD/$NAME-$VERSION.src.tar.* || exit 1
#this moves whatever was extracted to the std dirname we are expecting
mv * $NAME-$VERSION &> /dev/null 2>&1
mkdir -p $PKG
#-----------------------------------------------------

if [ "$CLANG" = "no" ]; then
  cd $NAME-${VERSION}
else
  ( cd $CWD
	for CLANGSRC in $(echo $CLANGLINK);do
		if [ ! -f $CWD/$(basename $CLANGSRC) ]
		then
			wget --no-check-certificate -c $CLANGSRC
		fi
	done ) 	
  cd $NAME-${VERSION}/tools
  rm -rf clang clang-${VERSION}.src
  tar xvf $CWD/clang-$VERSION.src.tar.gz
  mv clang-${VERSION}.src clang
  cd ../

  # clang fixes for slackware
  # No longer needed as of llvm/clang 3.0?
  #patch -p1 -d tools/clang -i $CWD/clang-slackware.diff
fi

cd $TMP/$NAME-$VERSION


#PATCHES
#-----------------------------------------------------
# Put any Patches here *NOTE this only works if all 
# your patches use the -p1 strip option!
#-----------------------------------------------------
for i in $CWD/patches/*;do
  patch -p1 <$i
  mkdir -p $PKG/usr/doc/$NAME-$VERSION/patches/
  cp $i $PKG/usr/doc/$NAME-$VERSION/patches/
done
#-----------------------------------------------------
 
 

#SET PERMISSIONS
#-----------------------------------------
echo "Setting permissions..."
chown -R root:root .
find . -perm 664 -exec chmod 644 {} \;
find . -perm 777 -exec chmod 755 {} \;
find . -perm 2777 -exec chmod 755 {} \;
find . -perm 775 -exec chmod 755 {} \;
find . -perm 2755 -exec chmod 755 {} \;
find . -perm 774 -exec chmod 644 {} \;
find . -perm 666 -exec chmod 644 {} \;
find . -perm 600 -exec chmod 644 {} \;
find . -perm 444 -exec chmod 644 {} \;
find . -perm 400 -exec chmod 644 {} \;
find . -perm 440 -exec chmod 644 {} \;
find . -perm 511 -exec chmod 755 {} \;
find . -perm 711 -exec chmod 755 {} \;
find . -perm 555 -exec chmod 755 {} \;
#-----------------------------------------



#CONFIGURE & MAKE
#----------------------------------------------------------------------
# If you are building a KDE-related app, then change the following
# arguments in the script below:
# --prefix=$(kde-config -prefix) \
# --sysconfdir=/etc/kde \
#
# Making these changes will ensure that your package will build in the
# correct path and that it will work seamlessly within the KDE environment.
#
#-----------------------------------------------------------------------

CINC="/usr/include/"
# force to use i586 instead of $ARCH due to gcc use i586 :(
GCCDIR=/usr/lib$LIBDIRSUFFIX/gcc/$CONFIGURE_TRIPLET*/*/ 
CINC="$CINC:$(echo ${GCCDIR})/include/"
CINC="$CINC:$(echo ${GCCDIR})/include-fixed/"

echo "Configuring source..."
./configure --prefix=/usr \
  --libdir=/usr/lib${LIBDIRSUFFIX} \
  --infodir=/usr/info \
  --sysconfdir=/etc \
  --localstatedir=/var \
  --mandir=/usr/man \
  --enable-optimized \
  --disable-assertions \
  --enable-pic \
  --with-c-include-dirs=$CINC \
  --program-prefix="" \
  --program-suffix="" \
  --build=$CONFIGURE_TRIPLET \
  --host=$CONFIGURE_TRIPLET \
  $CONFIG_OPTIONS || exit 1

# Correct libdir setting
sed -i "s|\$(PROJ_prefix)/lib|\$(PROJ_prefix)/lib$LIBDIRSUFFIX|" \
  Makefile.config

if [ "$CLANG" != "no" ]; then
  sed -i "s|\$(PROJ_prefix)/lib|\$(PROJ_prefix)/lib$LIBDIRSUFFIX|" \
    tools/clang/lib/Headers/Makefile
fi

make || exit 1

make install DESTDIR=$PKG || exit 1


#######################################################################
#Miscellenious tweaks and things outside a normal ./configure go here #
#######################################################################

# Fix hardcoded libdir
sed -i "s|\$(PROJ_prefix)/lib|\$(PROJ_prefix)/lib$LIBDIRSUFFIX|" \
	  Makefile.config
sed -i "s|\$(PROJ_prefix)/lib|\$(PROJ_prefix)/lib$LIBDIRSUFFIX|" \
	  tools/clang/lib/Headers/Makefile
sed -i "s|\"lib\"|\"lib${LIBDIRSUFFIX}\"|" \
	  tools/clang/lib/Frontend/CompilerInvocation.cpp
sed -i "s|\"lib\"|\"lib${LIBDIRSUFFIX}\"|" \
	  tools/clang/lib/Driver/Tools.cpp
sed -i "s|ActiveLibDir = ActivePrefix + \"/lib\"|ActiveLibDir = ActivePrefix + \"/lib${LIBDIRSUFFIX}\"|g" \
	  tools/llvm-config/llvm-config.cpp

if [ "$CLANG" != "no" ]; then
  # install clang-static-analyzer
  mkdir -p $PKG/usr/lib$LIBDIRSUFFIX/clang-analyzer
  cp -pr tools/clang/tools/scan-{build,view} \
    $PKG/usr/lib$LIBDIRSUFFIX/clang-analyzer/ || exit 1
  for i in scan-{build,view}; do
    ln -s /usr/lib$LIBDIRSUFFIX/clang-analyzer/$i/$i \
      $PKG/usr/bin/$i || exit 1
  done
  for i in ccc c++; do
    ln -s /usr/lib$LIBDIRSUFFIX/clang-analyzer/scan-build/$i-analyzer \
      $PKG/usr/bin/$i-analyzer || exit 1
  done
fi

# Remove example libraries
rm -f $PKG/usr/lib$LIBDIRSUFFIX/LLVMHello*

# Fix wrong libdir
sed -i -e "s|ABS_RUN_DIR/lib\"|ABS_RUN_DIR/lib$LIBDIRSUFFIX\"|" \
  $PKG/usr/bin/llvm-config

# Fix paths in scan-build
sed -i "s|\$RealBin/bin|/usr/bin|" \
	  $PKG/usr/lib$LIBDIRSUFFIX/clang-analyzer/scan-build/scan-build
sed -i "s|\$RealBin/sorttable.js|/usr/lib${LIBDIRSUFFIX}/clang-analyzer/scan-build/sorttable.js|" \
	  $PKG/usr/lib$LIBDIRSUFFIX/clang-analyzer/scan-build/scan-build
sed -i "s|\$RealBin/scanview.css|/usr/lib${LIBDIRSUFFIX}/clang-analyzer/scan-build/scanview.css|" \
	  $PKG/usr/lib$LIBDIRSUFFIX/clang-analyzer/scan-build/scan-build

mkdir -p $PKG/usr/doc/$NAME-$VERSION
cp -a CREDITS* README* LICENSE* $PKG/usr/doc/$NAME-$VERSION
cat $CWD/$NAME.SlackBuild > $PKG/usr/doc/$NAME-$VERSION/$NAME.SlackBuild

# move doc to the correct location
mv $PKG/usr/docs/llvm/* $PKG/usr/doc/$NAME-$VERSION
rm -rf $PKG/usr/docs

if [ "$CLANG" != "no" ]; then
  mkdir $PKG/usr/doc/$NAME-$VERSION/clang
  cp -a tools/clang/{INSTALL,NOTES,README,LICENSE}* \
    $PKG/usr/doc/$NAME-$VERSION/clang
fi

#----------------------------------------------------------------------

if [ -d $PKG/usr/share/man ];then
mkdir -p $PKG/usr/man
mv $PKG/usr/share/man/* $PKG/usr/man
rm -rf $PKG/usr/share/man
fi
find $PKG/usr/man -type f -exec gzip -9 {} \;

if [ -d $PKG/usr/share/info ];then
mkdir -p $PKG/usr/info
mv $PKG/usr/share/info/* $PKG/usr/info
rm -rf $PKG/usr/share/info
fi 
find $PKG/usr/info -type f -exec gzip -9 {} \;

mkdir -p $PKG/install
if [ -d $PKG/usr/info ];then
cat >> $PKG/install/doinst.sh << EOF
CWD=\$(pwd)
cd usr/info
if [ -f dir ];then
    rm dir
fi
if [ -f dir.gz ];then
    rm dir.gz
fi
for i in *.info.gz;do
    install-info \$i dir
done
cd \$CWD
EOF
fi

# Add schemas install to the doinst.sh if schemas are found.
if [ -d $PKG/etc/gconf/schemas ];then
# Make sure we have gconftool installed
echo "if [ -x usr/bin/gconftool-2 ]; then" >> $PKG/install/doinst.sh
( cd $PKG/etc/gconf/schemas
for schema in *.schemas; do
 # Install schemas
 echo "GCONF_CONFIG_SOURCE=\"xml::etc/gconf/gconf.xml.defaults\" \
   usr/bin/gconftool-2 --makefile-install-rule \
   etc/gconf/schemas/${schema} >/dev/null 2>&1" \
   >> $PKG/install/doinst.sh
done;
)
# Finish off gconf block
echo "fi" >> $PKG/install/doinst.sh
fi

cat >> $PKG/install/doinst.sh << EOF
# update rarian database
if [ -x usr/bin/rarian-sk-update ]; then
  usr/bin/rarian-sk-update 1> /dev/null 2> /dev/null
fi

# update mime database
if [ -x usr/bin/update-mime-database ]; then
  usr/bin/update-mime-database usr/share/mime 1> /dev/null 2> /dev/null
fi

# update desktop entries
if [ -x usr/bin/update-desktop-database ]; then
  usr/bin/update-desktop-database 1> /dev/null 2> /dev/null
fi

# update hicolor icons
if [ -e usr/share/icons/hicolor/icon-theme.cache ]; then
	rm -f usr/share/icons/hicolor/icon-theme.cache
fi
usr/bin/gtk-update-icon-cache -f -q usr/share/icons/hicolor 1>/dev/null 2>/dev/null

if [ -x /usr/bin/glib-compile-schemas ]; then
  /usr/bin/glib-compile-schemas usr/share/glib-2.0/schemas/ >/dev/null 2>&1
fi

# Restart gconfd-2 if running to reload new gconf settings
if ps acx | grep -q gconfd-2 ; then
        killall -HUP gconfd-2 ;
fi

# A good idea whenever kernel modules are added or changed:
if [ -x sbin/depmod ]; then
  /sbin/depmod -a 1> /dev/null 2> /dev/null
fi
EOF

#if there is a slack-desc in src dir use it
if test -f $CWD/slack-desc; then
cp $CWD/slack-desc $RELEASEDIR/slack-desc
else
# This creates the white space in front of "handy-ruler" in slack-desc below.

LENGTH=$(expr length "$NAME")
SPACES=0
SHIM=""
until [ "$SPACES" = "$LENGTH" ]; do
SHIM="$SHIM "
let SPACES=$SPACES+1
done

# Fill in the package summary between the () below.
# Then package the description, License, Author and Website.
# There may be no more then 11 $NAME: lines in a valid slack-desc.

cat > $RELEASEDIR/slack-desc << EOF
# HOW TO EDIT THIS FILE:
# The "handy ruler" below makes it easier to edit a package description.  Line
# up the first '|' above the ':' following the base package name, and the '|'
# on the right side marks the last column you can put a character in.  You must
# make exactly 11 lines for the formatting to be correct.  It's also
# customary to leave one space after the ':'.

$SHIM|-----handy-ruler------------------------------------------------------|
$NAME: $NAME (LLVM compiler toolkit)
$NAME:
$NAME: Low Level Virtual Machine is a toolkit for the construction of highly
$NAME: optimized compilers, optimizers, and runtime environments.
$NAME:
$NAME: This package also includes the clang frontend for the C family of
$NAME: languages:  C, C++, Objective-C, and Objective-C++
$NAME:
$NAME: License: Refer to LICENSE.TXT
$NAME: Authors: Refer to CREDITS.TXT
$NAME: Website: http://llvm.org/

EOF
fi
cat >> $RELEASEDIR/slack-desc << EOF



#----------------------------------------
BUILDDATE: $(date)
PACKAGER:  $VL_PACKAGER
HOST:      $(uname -srm)
DISTRO:    $(cat /etc/vector-version)
CFLAGS:    $CFLAGS
LDFLAGS:   $LDFLAGS
CONFIGURE: $(awk "/\\$\ \.\/configure\ /" $TMP/$DIRNAME/config.log)

EOF

cat $RELEASEDIR/slack-desc > $PKG/install/slack-desc

#STRIPPING
#------------------------------------------------------------------------------------------------------------------
cd $PKG
echo " "
echo "Stripping...."
echo " "
find . | xargs file | grep "executable" | grep ELF | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null
find . | xargs file | grep "shared object" | grep ELF | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null
#------------------------------------------------------------------------------------------------------------------


#FINISH PACKAGE
#--------------------------------------------------------------
echo "Finding dependencies..."
ADD="$ADDRB" EXCLUDE="$EXRB" requiredbuilder -v -y -s $RELEASEDIR $PKG
echo "Creating package $NAME-$VERSION-$ARCH-$BUILD.txz"
makepkg -l y -c n $RELEASEDIR/$NAME-$VERSION-$ARCH-$BUILD.txz

cd $CWD
echo "Cleaning up temp files..." && rm -rf $TMP
echo "Package Complete"
fi
#--------------------------------------------------------------

# vim: set tabstop=4 shiftwidth=4 foldmethod=marker : ##
