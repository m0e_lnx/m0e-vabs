#!/bin/sh

# Copyright 2007, 2008  Erik Hanson  erik@slackbuilds.org
# Copyright 2008, 2009, 2010  Patrick J. Volkerding, Sebeka, MN, USA
# All rights reserved.
#
# Redistribution and use of this script, with or without modification, is
# permitted provided that the following conditions are met:
#
# Redistributions of this script must retain the above copyright
# notice, this list of conditions and the following disclaimer.
#
#  THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED
#  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
#  MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO
#  EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
#  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
#  PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
#  OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
#  WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
#  OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
#  ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


NAME=pygobject
VERSION=${VERSION:-2.28.6}
BASEVER=$(echo $VERSION | cut -f 1-2 -d .)
BUILDNUM=${BUILDNUM:-2}
LINK=${LINK:-"ftp://ftp.gnome.org/pub/gnome/sources/$NAME/$BASEVER/$NAME-$VERSION.tar.bz2"}
NUMJOBS=${NUMJOBS:-" -j6 "}
VL_VERSION=${VL_VERSION:-"$(ls /var/log/packages/|grep vlconfig2|cut -d "-" -f4|cut -c 2-5)"}
BUILD=${BUILD:-"$BUILDNUM""$VL_VERSION"}
ARCH=${ARCH:-"$(uname -m)"}
CONFIG_OPTIONS=${CONFIG_OPTIONS:-""}
LDFLAG_OPTIONS=${LDFLAG_OPTIONS:-""}
ADDRB=${ADDRB:-"pycairo >= 1.8.10"} #Add deps that need to be added to the slack-required file here
EXRB=${EXRB:-""} #Add deps that need to be excluded from the slack-required file here
MAKEDEPENDS=${MAKEDEPENDS:-"dbus dbus-python dbus-glib pycairo"}

# Automatically determine the architecture we're building on:
if [ -z "$ARCH" ]; then
  case "$( uname -m )" in
    i?86) export ARCH=i586 ;;
    arm*) export ARCH=arm ;;
    # Unless $ARCH is already set, use uname -m for all other archs:
       *) export ARCH=$( uname -m ) ;;
  esac
fi

if [ "$NORUN" != 1 ]; then

#CFLAGS SETUP
#--------------------------------------------
if [[ "$ARCH" = i?86 ]]; then
  ARCH=i586
  SLKCFLAGS="-O2 -march=i586 -mtune=i686"
  CONFIGURE_TRIPLET="i586-vector-linux"
  LIBDIRSUFFIX=""
elif [ "$ARCH" = "x86_64" ]; then
  SLKCFLAGS="-O2 -fpic"
  CONFIGURE_TRIPLET="x86_64-vlocity-linux"
  LIBDIRSUFFIX="64"
elif [ "$ARCH" = "powerpc" ]; then
  SLKCFLAGS="-O2"
  CONFIGURE_TRIPLET="powerpc-vlocity-linux"
  LIBDIRSUFFIX=""
fi

export CFLAGS="$SLKCFLAGS $CFLAG_OPTIONS"
export CXXFLAGS=$CFLAGS
export LDFLAGS="$LDFLAGS $LDFLAG_OPTIONS"

#get the source..........
for SRC in $(echo $LINK);do
if [ ! -f $CWD/$(basename $SRC) ]
then
	wget --no-check-certificate -c $SRC
fi
done
CWD=$(pwd)
cd ../
RELEASEDIR=$(pwd)
cd $CWD
mkdir -p $RELEASEDIR/tmp
TMP=$RELEASEDIR/tmp
PKG=$TMP/package-$NAME

rm -rf $PKG
mkdir -p $TMP $PKG
cd $TMP 
rm -rf $NAME-$VERSION
tar xvf $CWD/$NAME-$VERSION.tar.bz2 || exit 1
cd $NAME-$VERSION || exit 1
chown -R root:root .
find . \
  \( -perm 777 -o -perm 775 -o -perm 711 -o -perm 555 -o -perm 511 \) \
  -exec chmod 755 {} \; -o \
  \( -perm 666 -o -perm 664 -o -perm 600 -o -perm 444 -o -perm 440 -o -perm 400 \) \
  -exec chmod 644 {} \;

patch -Np1 --verbose < $CWD/patches/pygobject-2.28.6-introspection-1.patch || exit 1

CFLAGS="$SLKCFLAGS" \
./configure \
  --prefix=/usr \
  --libdir=/usr/lib${LIBDIRSUFFIX} \
  --sysconfdir=/etc \
  --localstatedir=/var \
  --enable-introspection \
  --build=$CONFIGURE_TRIPLET $CONFIG_OPTIONS || exit 1

make $NUMJOBS || exit 1
make install DESTDIR=$PKG || exit 1

find $PKG | xargs file | grep -e "executable" -e "shared object" \
  | grep ELF | cut -f 1 -d : | xargs strip --strip-unneeded 2> /dev/null

mkdir -p $PKG/usr/doc/$NAME-$VERSION
cp -a \
  INSTALL AUTHORS COPYING* NEWS README* \
    $PKG/usr/doc/$NAME-$VERSION
( cd $PKG/usr/doc/$NAME-$VERSION 
  ln -s ../../share/gtk-doc/html/pygobject html 
)

# If there's a ChangeLog, installing at least part of the recent history
# is useful, but don't let it get totally out of control:
if [ -r ChangeLog ]; then
  DOCSDIR=$(echo $PKG/usr/doc/${NAME}-$VERSION)
  cat ChangeLog | head -n 1000 > $DOCSDIR/ChangeLog
  touch -r ChangeLog $DOCSDIR/ChangeLog
fi

mkdir -p $PKG/install
cat $CWD/slack-desc > $PKG/install/slack-desc

cd $PKG
echo "Finding dependencies..."
ADD="$ADDRB" EXCLUDE="$EXRB" requiredbuilder -v -y -s $RELEASEDIR $PKG
echo "Creating package $NAME-$VERSION-$ARCH-$BUILD.txz"
makepkg -c n -l y $RELEASEDIR/$NAME-$VERSION-$ARCH-$BUILD.txz

rm -rf $TMP
fi
